# Angel #
Android application that intercepts text and images in selected Android applications. 

## How it works? ##
By intercepting the usage of common UI elements like TextViews and ImageViews (among others). 
### Doesnt it sent too much data? ###
For now yes it does, its "RT". BUT at least we only send each thing once. Text is hashed and compared to see if was already sent. Images are compressed and hashed for comparison as well. 
### Doesnt it slow down the application? ###
Nothing is sent twice and it is pretty lightweight on the device, no perceptible delays. Try it yourself :)

## Dependencies ##
On web server side:
```
sudo apt-get install nodejs
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo npm install -g bower
sudo npm install -g express
```

## How to install? ##
* Make sure you have root on your device (it would either be root, app rewriting or os image, we choose root).
* Install the xposed framework in your device. (This framework replaces your app_process which launches the Dalvik VM with some special hooks for methods)
* Install the Angel app present in the Download section (or build the project).
* Plug cable to device and do "adb shell" on the device. Create file "data/local/tmp/config" with the following content

```
#!java

IP <some_ipOrHostname_here>
TYPE CLIENT
```

* Enable its module on Xposed.
* Go the web server (<some_ipOrHostname_here> in the config file)
* Install and run a mosquitto server (Message queueing server with MQTT protocol)
* copy the web/ folder there
* Do npm install (in main folder) and [bower](https://www.npmjs.com/package/bower) install (public folder)
* Launch app.js (node app.js) and it should be listening on port 3000.
* Just use the browser on any device to visit it :)