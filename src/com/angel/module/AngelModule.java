package com.angel.module;

import java.util.HashMap;

import android.util.Log;

import com.angel.apps.AppModule;
import com.angel.apps.BrowserModule;
import com.angel.apps.SkypeModule;
import com.angel.apps.StandardModule;
import com.angel.apps.YTModule;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class AngelModule implements IXposedHookLoadPackage {

	public static final String TAG = "Angel";

	public HashMap<String, AppModule> _knownApps = new HashMap<String, AppModule>();

	
	public AngelModule() {
		
		AppModule[] apps = {new StandardModule("com.facebook.orca", ""), 
				new StandardModule("com.whatsapp", ""),
				new YTModule(),
				new SkypeModule(),
				//new StandardModule("com.skype.raider", ""),
				new BrowserModule()};//{ new FBModule(), new SkypeModule(), new StandardModule("com.whatsapp","")};

		for (AppModule app : apps){
			_knownApps.put(app.getAppName(), app);
			Log.v(TAG,"Inserting " + app.getAppName());
		}
			
		for(AppModule app : _knownApps.values()){
			Log.v(TAG, "---->" + app.getAppName());
		}
	}

	@Override
	public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable {
		Log.v(TAG, "->" + lpparam.packageName);

		AppModule module = _knownApps.get(lpparam.packageName);

		if (module != null) {
			module.handle(lpparam);
		}
	}

}
