package com.angel.angeldemo;

import java.util.Date;

import com.angel.functionalities.BootReceiver;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	    AlarmManager am = (AlarmManager) this
	            .getSystemService(Context.ALARM_SERVICE);

	    Date futureDate = new Date(new Date().getTime() + 1000 * 1 * 15);

//	    futureDate.setHours(8);
//	    futureDate.setMinutes(0);
//	    futureDate.setSeconds(0);

	    Intent intent = new Intent(this, BootReceiver.class);
	    intent.putExtra("isShutdown", true);
	    intent.setAction("BananaMaster");

	    PendingIntent sender = PendingIntent.getBroadcast(this, 0, intent,
	            PendingIntent.FLAG_UPDATE_CURRENT);
	    
	    am.set(AlarmManager.RTC_WAKEUP,futureDate.getTime(), sender); 
	}
}
