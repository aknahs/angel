package com.angel.angeldemo;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.angel.module.AngelModule;

import android.R.color;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CurfewActivity extends Activity {
	public String mAppName = "Curfew";
	public String mApkName = "Curfew";
	public String mMessage = "This device is under curfew and will shutdown in a few moments";
	public Bitmap mBitmap = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		generateActivityUI();
	}

	public void generateActivityUI() {
		
		loadImages();

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		LinearLayout layout = new LinearLayout(this);
		layout.setGravity(Gravity.CENTER);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.setBackgroundColor(color.holo_blue_bright);

		// Set generic layout parameters
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		ImageView iv = new ImageView(this);
		TextView titleView = new TextView(this);
		TextView messageView = new TextView(this);

		titleView.setText(mAppName);
		messageView.setText(mMessage);
		titleView.setGravity(Gravity.CENTER_VERTICAL
				| Gravity.CENTER_HORIZONTAL);
		messageView.setGravity(Gravity.CENTER_VERTICAL
				| Gravity.CENTER_HORIZONTAL);

		titleView.setTextSize(24);
		messageView.setTextSize(20);

		List<PackageInfo> pkgs = getPackageManager().getInstalledPackages(0);

		PackageInfo pckInfo = null;
		for (PackageInfo p : pkgs) {
			if (p.packageName.equals(mApkName)) {
				pckInfo = p;
				break;
			}
		}

		Drawable drawable = null;

		if (mBitmap == null) {
			if (pckInfo == null)
				drawable = getFullResDefaultActivityIcon();
			else {
				Log.v(AngelModule.TAG, "Found packageInfo and activityInfo");
				ActivityInfo app;
				try {
					app = getPackageManager().getActivityInfo(
							this.getComponentName(),
							PackageManager.GET_ACTIVITIES
									| PackageManager.GET_META_DATA);

					drawable = getFullResIcon(app);
					// drawable = pckInfo.applicationInfo
					// .loadIcon(getPackageManager());
					// drawable = getFullResIcon(pckInfo.activities[0]);
				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		} else {
			Log.v(AngelModule.TAG, "loading web bitmap");
			drawable = new BitmapDrawable(getResources(), mBitmap);
		}

		if (drawable == null)
			drawable = getFullResDefaultActivityIcon();

		iv.setImageDrawable(drawable);
		// iv.setImageBitmap(bitmap);
		// iv.setScaleType(ScaleType.FIT_XY);
		// iv.setScaleX(2);
		// iv.setScaleY(2);

		// lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

		layout.addView(titleView, lp);
		layout.addView(iv, lp);
		layout.addView(messageView, lp);

		setContentView(layout);

	}

	public void loadImages() {
		Thread imThr;

		if (mBitmap == null) {
			imThr = new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					mBitmap = getImage("https://dl.dropboxusercontent.com/u/8100253/angel/time.png");
				}
			});

			imThr.start();

			try {
				imThr.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	// http://upload.wikimedia.org/wikipedia/commons/4/4e/Telecom-icon.svg
	// http://commons.wikimedia.org/wiki/File:Flat_UI_-_time.png
	// http://iconpopanswers.com/wp-content/uploads/2013/04/iconpopmania-163.jpg

	// https://dl.dropboxusercontent.com/u/8100253/angel/internet.jpg
	// https://dl.dropboxusercontent.com/u/8100253/angel/shallnotpass.jpg
	// https://dl.dropboxusercontent.com/u/8100253/angel/time.png

	private Bitmap getImage(String urlstr) {
		URL url;
		try {
			url = new URL(urlstr);
			InputStream in = new BufferedInputStream(url.openStream());
			// ByteArrayOutputStream out = new ByteArrayOutputStream();
			// byte[] buf = new byte[1024];
			// int n = 0;
			// while (-1 != (n = in.read(buf))) {
			// out.write(buf, 0, n);
			// }
			// out.close();
			// in.close();
			// byte[] response = out.toByteArray();

			return BitmapFactory.decodeStream(in);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public Drawable getFullResDefaultActivityIcon() {
		return getFullResIcon(Resources.getSystem(),
				android.R.mipmap.sym_def_app_icon);
	}

	public Drawable getFullResIcon(Resources resources, int iconId) {
		Drawable d;
		try {
			ActivityManager activityManager = (ActivityManager) this
					.getSystemService(Context.ACTIVITY_SERVICE);
			int iconDpi = activityManager.getLauncherLargeIconDensity();
			d = resources.getDrawableForDensity(iconId, iconDpi);
		} catch (Resources.NotFoundException e) {
			d = null;
		}

		return (d != null) ? d : getFullResDefaultActivityIcon();
	}

	public Drawable getFullResIcon(String packageName, int iconId) {
		Resources resources;
		try {
			resources = this.getPackageManager().getResourcesForApplication(
					packageName);
		} catch (PackageManager.NameNotFoundException e) {
			resources = null;
		}
		if (resources != null) {
			if (iconId != 0) {
				return getFullResIcon(resources, iconId);
			}
		}
		return getFullResDefaultActivityIcon();
	}

	public Drawable getFullResIcon(ResolveInfo info) {
		return getFullResIcon(info.activityInfo);
	}

	public Drawable getFullResIcon(ActivityInfo info) {
		Resources resources;
		try {
			resources = this.getPackageManager().getResourcesForApplication(
					info.applicationInfo);
		} catch (PackageManager.NameNotFoundException e) {
			resources = null;
		}
		if (resources != null) {
			int iconId = info.getIconResource();
			if (iconId != 0) {
				return getFullResIcon(resources, iconId);
			}
		}
		return getFullResDefaultActivityIcon();
	}

	@SuppressWarnings("unused")
	private Drawable getAppIcon(ResolveInfo info) {
		return getFullResIcon(info.activityInfo);
	}

}
