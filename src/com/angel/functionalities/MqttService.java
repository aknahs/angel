package com.angel.functionalities;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.Toast;

import com.angel.angeldemo.MainActivity;
import com.angel.angeldemo.R;

/*This implementation is broken and should be replaced by IBM examples*/
public class MqttService extends Service implements MqttCallback {
	public static final String DEBUG_TAG = "Mqtt-Service"; // Debug TAG

	private static final String MQTT_THREAD_NAME = "MqttService[" + DEBUG_TAG
			+ "]"; // Handler Thread ID

	private static volatile String MQTT_BROKER = "tid.system-ns.net"; // Broker
																		// URL
	// or IP
	// Address
	private static final int MQTT_PORT = 1883; // Broker Port

	public static final int MQTT_QOS_0 = 0; // QOS Level 0 ( Delivery Once no
											// confirmation )
	public static final int MQTT_QOS_1 = 1; // QOS Level 1 ( Delevery at least
											// Once with confirmation )
	public static final int MQTT_QOS_2 = 2; // QOS Level 2 ( Delivery only once
											// with confirmation with handshake
											// )

	private static final int MQTT_KEEP_ALIVE = 30000;// 240000; // KeepAlive
														// Interval in
														// MS (4 minutes)

	private static final String MQTT_KEEP_ALIVE_TOPIC_FORAMT = "/users/%s/keepalive"; // Topic
																						// format
																						// for
																						// KeepAlives
	private static final byte[] MQTT_KEEP_ALIVE_MESSAGE = { 0 }; // Keep Alive
																	// message
																	// to send
	private static final int MQTT_KEEP_ALIVE_QOS = MQTT_QOS_0; // Default
																// Keepalive QOS

	private static final boolean MQTT_CLEAN_SESSION = true; // Start a clean
															// session?

	private static final String MQTT_URL_FORMAT = "tcp://%s:%d"; // URL Format
																	// normally
																	// don't
																	// change

	public static final String ACTION_BOOT = DEBUG_TAG + ".BOOT";

	private static final String ACTION_START = DEBUG_TAG + ".START"; // Action
																		// to
																		// start
	private static final String ACTION_STOP = DEBUG_TAG + ".STOP"; // Action to
																	// stop
	private static final String ACTION_KEEPALIVE = DEBUG_TAG + ".KEEPALIVE"; // Action
																				// to
																				// keep
																				// alive
																				// used
																				// by
																				// alarm
																				// manager
	private static final String ACTION_RECONNECT = DEBUG_TAG + ".RECONNECT"; // Action
																				// to
																				// reconnect

	private static final String DEVICE_ID_FORMAT = "andr_%s"; // Device ID
																// Format, add
																// any prefix
																// you'd like
																// Note: There
																// is a 23
																// character
																// limit you
																// will get
																// An NPE if you
																// go over that
																// limit
	private boolean mStarted = false; // Is the Client started?
	private static String mDeviceId; // Device ID, Secure.ANDROID_ID
	private Handler mConnHandler; // Separate Handler thread for networking

	private MemoryPersistence mDataStore; // Defaults to FileStore//
											// MqttDefaultFilePersistence
	private MemoryPersistence mMemStore; // On Fail reverts to MemoryStore
	private MqttConnectOptions mOpts; // Connection Options

	private MqttTopic mKeepAliveTopic; // Instance Variable for Keepalive topic

	private static MqttClient mClient; // Mqtt Client

	private AlarmManager mAlarmManager; // Alarm manager to perform repeating
										// tasks
	private ConnectivityManager mConnectivityManager; // To check for
														// connectivity changes

	public MqttService() {
		mMessenger = new Messenger(new IncomingHandler(this));
	}

	public static enum MqttMessageType {
		BIND, PUBLISH, SUBSCRIBE, REGISTER
	}

	/**
	 * Target we publish for clients to send messages to IncomingHandler. Note
	 * that calls to its binder are sequential!
	 */
	protected Messenger mMessenger;

	private static final String MQTT_FROM_APK_TOPIC_FORMAT = "/users/%s/%s/frommobile";

	/**
	 * mClassContext When binding to the service, we return an interface to our
	 * messenger for sending messages to the service.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		Log.v(DEBUG_TAG, "Process binded to this service"); // TODO: add source
															// to intent extra
															// field
		return mMessenger.getBinder();
	}

	/**
	 * Contains the apk topics for each messenger, needed for defining the topic
	 * on publish
	 */
	public static HashMap<String, MqttTopic> mApkTopics = new HashMap<String, MqttTopic>();

	/**
	 * Handler of incoming messages from clients.
	 */
	static class IncomingHandler extends Handler {

		MqttService mService;

		public IncomingHandler(MqttService service) {
			mService = service;
		}

		private ArrayList<Message> storedMsg = new ArrayList<Message>();

		@Override
		public void handleMessage(Message msg) {
			String apkname = msg.getData().getString("apk");

			if (msg.what == MqttMessageType.BIND.ordinal()) {
				// Since the messenger always comes associated with the message
				// this is probably not needed.
				Log.v(DEBUG_TAG,
						"-------------------MQTT BIND-----------------------");

				Log.v(DEBUG_TAG, "Registering messenger for message : "
						+ msg.what + " apk : " + apkname);

				Log.v(DEBUG_TAG, "Registering userID : " + mDeviceId);

				if (mClient == null) {
					Log.v(DEBUG_TAG, "XXX mClient was null!");
				}

				MqttTopic fromtop = mClient.getTopic(String.format(Locale.US,
						MQTT_FROM_APK_TOPIC_FORMAT, mDeviceId, apkname));

				mApkTopics.put(apkname, fromtop);

			} else if (msg.what == MqttMessageType.PUBLISH.ordinal()) {
				try {
					Log.v(DEBUG_TAG,
							"-------------------MQTT PUBLISH-----------------------");

					Log.v(DEBUG_TAG,
							"Publishing TrapMessageComposite with message id : "
									+ msg.arg1 + " : to : "
									+ mApkTopics.get(apkname).getName());

					MqttMessage message = new MqttMessage(msg.getData()
							.getString("msg").getBytes());

					message.setQos(0);

					mApkTopics.get(apkname).publish(message);

					Log.v(DEBUG_TAG,
							"-------------------\\MQTT PUBLISH-----------------------");

				} catch (MqttPersistenceException e) {
					e.printStackTrace();
				} catch (MqttException e) {
					Log.v(DEBUG_TAG,
							"Will store message to send when there is connection");
					storedMsg.add(msg);
					e.printStackTrace();
					mService.connectionLost(e);

				}
			}

		}
	}

	/**
	 * Start MQTT Client
	 * 
	 * @param Context
	 *            context to start the service with
	 * @return void
	 */
	public static void actionStart(Context ctx) {
		Intent i = new Intent(ctx, MqttService.class);
		i.setAction(ACTION_START);
		ctx.startService(i);
	}

	/**
	 * Stop MQTT Client
	 * 
	 * @param Context
	 *            context to start the service with
	 * @return void
	 */
	public static void actionStop(Context ctx) {
		Intent i = new Intent(ctx, MqttService.class);
		i.setAction(ACTION_STOP);
		ctx.startService(i);
	}

	/**
	 * Send a KeepAlive Message
	 * 
	 * @param Context
	 *            context to start the service with
	 * @return void
	 */
	public static void actionKeepalive(Context ctx) {
		Intent i = new Intent(ctx, MqttService.class);
		i.setAction(ACTION_KEEPALIVE);
		ctx.startService(i);
	}

	/**
	 * Initalizes the DeviceId and most instance variables Including the
	 * Connection Handler, Datastore, Alarm Manager and ConnectivityManager.
	 */
	@Override
	public void onCreate() {
		super.onCreate();

		mDeviceId = String.format(DEVICE_ID_FORMAT,
				Secure.getString(getContentResolver(), Secure.ANDROID_ID));

		Log.i(DEBUG_TAG, "Device ID : " + mDeviceId);

		HandlerThread thread = new HandlerThread(MQTT_THREAD_NAME);
		thread.start();

		mConnHandler = new Handler(thread.getLooper());

		mDataStore = new MemoryPersistence();// new
												// MqttDefaultFilePersistence(getCacheDir().getAbsolutePath());

		mOpts = new MqttConnectOptions();
		mOpts.setCleanSession(MQTT_CLEAN_SESSION);

		mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
	}

	/**
	 * Service onStartCommand Handles the action passed via the Intent
	 * 
	 * @return START_REDELIVER_INTENT
	 */
	@SuppressWarnings("deprecation")
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);

		String action = intent.getAction();

		Log.i(DEBUG_TAG, "Received action of " + action);

		if (action == null) {
			Log.i(DEBUG_TAG,
					"Starting service with no action\n Probably from a crash");
		} else {
			if (action.equals(ACTION_START)) {
				Log.i(DEBUG_TAG, "Received ACTION_START");
				start();
			} else if (action.equals(ACTION_STOP)) {
				stop();
			} else if (action.equals(ACTION_KEEPALIVE)) {
				keepAlive();
			} else if (action.equals(ACTION_RECONNECT)) {
				if (isNetworkAvailable()) {
					reconnectIfNecessary();
				}
			} else if (action.equals(ACTION_BOOT)) {
				Log.i(DEBUG_TAG, "Received ACTION_BOOT");

				start();

				/*
				 * Notification noti = MqttServiceNotification.getNotification(
				 * mContext, mClassContext, mServiceTitle, mServiceMessage);
				 */

				Notification note = new Notification(R.drawable.ic_launcher,
						"TrapMaster-Engine", System.currentTimeMillis());

				// The PendingIntent to launch our activity if the user selects
				// this notification
				PendingIntent contentIntent = PendingIntent.getActivity(this,
						0, new Intent(this, MainActivity.class), 0);

				// Set the info for the views that show in the notification
				// panel.
				note.setLatestEventInfo(this, "TrapMaster-Engine",
						"Running the Mqtt Service like a boss!", contentIntent);

				note.flags |= Notification.FLAG_NO_CLEAR;

				startForeground(1337, note);
			}
		}

		/*
		 * We want our intents to be redelivered upon restart. Sticky would run
		 * onStartCommand with a null intent, with current implementation we are
		 * not treating this case although it would be easy. I guess the
		 * behavior would be similar (but service would already be initialized?)
		 */
		return START_REDELIVER_INTENT;
	}

	private volatile Object syncStart = new Object();

	/**
	 * Attempts connect to the Mqtt Broker and listen for Connectivity changes
	 * via ConnectivityManager.CONNECTVITIY_ACTION BroadcastReceiver
	 */
	private void start() {

		synchronized (syncStart) {

			if (mStarted) {
				Log.i(DEBUG_TAG, "Attempt to start while already started");
				return;
			}

			if (hasScheduledKeepAlives()) {
				stopKeepAlives();
			}

			connect();

		}
	}

	/**
	 * Attempts to stop the Mqtt client as well as halting all keep alive
	 * messages queued in the alarm manager
	 */
	private synchronized void stop() {
		if (!mStarted) {
			Log.i(DEBUG_TAG, "Attemtpign to stop connection that isn't running");
			return;
		}

		if (mClient != null) {
			mConnHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						mClient.disconnect();
					} catch (MqttException ex) {
						ex.printStackTrace();
					}
					//mClient = null;
					mStarted = false;

					stopKeepAlives();
				}
			});
		}

		stopForeground(true);
	}

	/**
	 * Connects to the broker with the appropriate datastore
	 */
	private void connect() {

		mConnHandler.post(new Runnable() {
			@Override
			public void run() {

				synchronized (syncStart) {

					if (mStarted && mClient != null) {
						return;
					}

					Log.v("MqttService", "Connecting service...");
					try {
						InetAddress address;
						try {
							while (!isNetworkAvailable()) {
								Log.v("Mqtt",
										"please connect to internet asap :D");
								Thread.sleep((long) 2);
							}
							address = InetAddress.getByName(MQTT_BROKER);
							MQTT_BROKER = address.getHostName();
						} catch (UnknownHostException e) {
							Log.v("MqttService", "Couldnt resolve address");
							e.printStackTrace();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

						String url = String.format(Locale.US, MQTT_URL_FORMAT,
								MQTT_BROKER, MQTT_PORT);

						Log.i(DEBUG_TAG, "Connecting with URL: " + url);
						try {
							if (mDataStore != null) {
								Log.i(DEBUG_TAG, "Connecting with DataStore");
								mClient = new MqttClient(url, mDeviceId,
										mDataStore);

							} else {
								Log.i(DEBUG_TAG, "Connecting with MemStore");
								mClient = new MqttClient(url, mDeviceId,
										mMemStore);
							}
						} catch (MqttException e) {
							e.printStackTrace();
						}

						mClient.connect(mOpts);

						mClient.setCallback(MqttService.this);

						mStarted = true; // Service is now connected

						Log.i(DEBUG_TAG,
								"Successfully connected and subscribed starting keep alives");
						
						Toast.makeText(getApplicationContext(), "Mqtt connected" , Toast.LENGTH_LONG).show();

						startKeepAlives();
					} catch (MqttException e) {
						if (!isNetworkAvailable())
							Log.v("MqttService",
									"connect failed since network connection was down. Will reconnect upon network change : "
											+ e);
						else {
							Log.v("MqttService",
									"connection was available but mqttservice failed");
							e.printStackTrace();
						}
					}
				}
			}
		});

	}

	/**
	 * Schedules keep alives via a PendingIntent in the Alarm Manager
	 */
	private void startKeepAlives() {
		Intent i = new Intent();
		i.setClass(this, MqttService.class);
		i.setAction(ACTION_KEEPALIVE);
		PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
		mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis() + MQTT_KEEP_ALIVE, MQTT_KEEP_ALIVE,
				pi);
	}

	/**
	 * Cancels the Pending Intent in the alarm manager
	 */
	private void stopKeepAlives() {
		Intent i = new Intent();
		i.setClass(this, MqttService.class);
		i.setAction(ACTION_KEEPALIVE);
		PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
		mAlarmManager.cancel(pi);
	}

	/**
	 * Publishes a KeepALive to the topic in the broker
	 */
	private synchronized void keepAlive() {
		if (isConnected()) {
			try {
				sendKeepAlive();
				return;
			} catch (MqttConnectivityException ex) {
				ex.printStackTrace();
				reconnectIfNecessary();
			} catch (MqttPersistenceException ex) {
				ex.printStackTrace();
				stop();
			} catch (MqttException ex) {
				ex.printStackTrace();
				stop();
			}
		}
	}

	/**
	 * Checkes the current connectivity and reconnects if it is required.
	 */
	public void reconnectIfNecessary() {
		if (isNetworkAvailable() && (!mStarted || mClient == null)) { /*
																	 * Avoid
																	 * locks on
																	 * send
																	 */
			reconnect();
		}
	}

	public synchronized void reconnect() { /*
											 * Would be better to have fine
											 * grained locking
											 */
		synchronized (syncStart) {

			if (isNetworkAvailable() && (!mStarted || mClient == null)) {
				connect();
			}
		}
	}

	/**
	 * Query's the NetworkInfo via ConnectivityManager to return the current
	 * connected state
	 * 
	 * @return boolean true if we are connected false otherwise
	 */
	private boolean isNetworkAvailable() {
		NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();

		return (info == null) ? false : info.isConnected();
	}

	/**
	 * Verifies the client State with our local connected state
	 * 
	 * @return true if its a match we are connected false if we aren't connected
	 */
	private boolean isConnected() {
		if (mStarted && mClient != null && !mClient.isConnected()) {
			Log.i(DEBUG_TAG,
					"Mismatch between what we think is connected and what is connected");
		}

		if (mClient != null) {
			return (mStarted && mClient.isConnected()) ? true : false;
		}

		return false;
	}

	public boolean receiverRegistered = false;

	/**
	 * Sends a Keep Alive message to the specified topic
	 * 
	 * @see MQTT_KEEP_ALIVE_MESSAGE
	 * @see MQTT_KEEP_ALIVE_TOPIC_FORMAT
	 * @return MqttDeliveryToken specified token you can choose to wait for
	 *         completion
	 */
	private MqttDeliveryToken sendKeepAlive() throws MqttConnectivityException,
			MqttPersistenceException, MqttException {

		if (!isConnected())
			throw new MqttConnectivityException();

		if (mKeepAliveTopic == null) {
			mKeepAliveTopic = mClient.getTopic(String.format(Locale.US,
					MQTT_KEEP_ALIVE_TOPIC_FORAMT, mDeviceId));
		}

		Log.i(DEBUG_TAG, "Sending Keepalive to " + MQTT_BROKER);

		MqttMessage message = new MqttMessage(MQTT_KEEP_ALIVE_MESSAGE);
		message.setQos(MQTT_KEEP_ALIVE_QOS);

		return mKeepAliveTopic.publish(message);
	}

	/**
	 * Query's the AlarmManager to check if there is a keep alive currently
	 * scheduled
	 * 
	 * @return true if there is currently one scheduled false otherwise
	 */
	private boolean hasScheduledKeepAlives() {
		Intent i = new Intent();
		i.setClass(this, MqttService.class);
		i.setAction(ACTION_KEEPALIVE);
		PendingIntent pi = PendingIntent.getBroadcast(this, 0, i,
				PendingIntent.FLAG_NO_CREATE);

		return (pi != null) ? true : false;
	}

	/**
	 * Connectivity Lost from broker
	 */
	@Override
	public void connectionLost(Throwable arg0) {

		Log.v("MqttService", "Connection lost! <-----------------");
		stopKeepAlives();

		//mClient = null;

		if (isNetworkAvailable()) {
			reconnectIfNecessary();
		}
	}

	/**
	 * MqttConnectivityException Exception class
	 */
	private class MqttConnectivityException extends Exception {
		private static final long serialVersionUID = -7385866796799469420L;
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		Log.i(DEBUG_TAG, "Message delivered " + arg0.getMessageId());
	}

	// /users/andr_5345f56420f86d59/com.aknahs.applicationa/tomobile
	// /users/andr_5345f56420f86d59/com.aknahs.applicationa/tomobile

	@Override
	public void messageArrived(String topic, MqttMessage message)
			throws Exception {
		/* for now we dont receive answers :D */
	}

}