package com.angel.functionalities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

public class MqttConnectorEmptyActivity extends Activity {

	public final static String TAG = "TRAP-BACKEND";
	private MqttAppConnector _appConnector;
	public static Bitmap bitmap = null;

	private static Class<?> _nextActivity = null;

	public MqttConnectorEmptyActivity(MqttAppConnector con, Class<?> activity) {
		_appConnector = con;
		_nextActivity = activity;
	}

	private class LaunchActivityTask extends AsyncTask<Void, Void, Void> {

		MqttConnectorEmptyActivity thisActivity;
		Class<?> launchActivity;
		MqttAppConnector connector;

		public LaunchActivityTask(MqttConnectorEmptyActivity thisAct,
				MqttAppConnector con, Class<?> act) {
			thisActivity = thisAct;
			launchActivity = act;
			connector = con;
		}

		@Override
		protected Void doInBackground(Void... params) {

			while (!connector.isBound()) {
				// try {
				// Thread.sleep(1);
				// } catch (InterruptedException e) {
				// break;
				// }
			}

			thisActivity.launchActivity(launchActivity);

			return null;
		}
	}

	public void launchActivity(Class<?> activity) {
		Intent intent = new Intent(this, activity);
		startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.v("MqttConnectorEmptyActivity",
				"Running Empty Activity. Binding to Service");

		generateActivityUI();

		if (!_appConnector.isBound()) {
			if (!_appConnector.bindService(((Context) this))) {
				Log.v("MqttConnectorEmptyActivity",
						"XXX Failed to bind to service");
			} else {
				Log.v("MqttConnectorEmptyActivity",
						"Successfully binded to service");
			}
		}

		Log.v("MqttConnectorEmptyActivity", "Launching next activity");

		new LaunchActivityTask(this, _appConnector, _nextActivity).execute();

	}

	private void generateActivityUI() {

		setTheme(android.R.style.Theme_NoDisplay);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		LinearLayout layout = new LinearLayout(this);
		layout.setGravity(Gravity.CENTER);
		layout.setOrientation(LinearLayout.VERTICAL);

		setContentView(layout);

	}
}