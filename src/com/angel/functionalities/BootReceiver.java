package com.angel.functionalities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.angel.angeldemo.CurfewActivity;

public class BootReceiver extends BroadcastReceiver {

	/* Starts the MqttService and the engine on boot */
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			Intent i = new Intent(context, MqttService.class);
			i.setAction(MqttService.ACTION_BOOT);
			context.startService(i);
		} else{
			
			if(intent.getAction().equals("BananaMaster") || intent.getBooleanExtra("isShutdown", false) == true){
				
				Intent i = new Intent();
		        i.setClassName("com.angel.angeldemo", "com.angel.angeldemo.CurfewActivity");
		        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		        context.startActivity(i);
				
//				try {
//				    Process proc = Runtime.getRuntime()
//				                    .exec(new String[]{ "su", "-c", "reboot" });
//				    proc.waitFor();
//				} catch (Exception ex) {
//				    ex.printStackTrace();
//				}
			}
		}
	}

}