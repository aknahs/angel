package com.angel.functionalities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

import android.util.Log;

import com.angel.module.AngelModule;

import de.robv.android.xposed.XC_MethodHook;

public class FunctionHook extends XC_MethodHook {
	
	private Method mFunction;
	private Object[] mParameters;
	
	public FunctionHook(Method function, Object[] parameters) {
		mFunction = function;
		mParameters = parameters;
	}
	
	@Override
	protected void afterHookedMethod(MethodHookParam param) {

		if (mFunction != null) {
			try {
				if (mParameters != null) {
					ArrayList<Object> temp = new ArrayList<Object>(
							Arrays.asList(mParameters));
					temp.add(0, param);
					mFunction.invoke(null, temp.toArray());
				} else
					mFunction.invoke(null, param);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		} else {
			Log.v(AngelModule.TAG, "function was null! Method : " + param.method.getName());
		}

	}
}
