package com.angel.functionalities;

import java.io.ByteArrayOutputStream;

import com.angel.apps.AppModule;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Base64;
import android.util.Log;

public class CompressAndSendImageRunnable implements Runnable {
	public Bitmap mBitmap;
	public MqttAppConnector mAppConnector;

	public CompressAndSendImageRunnable(Bitmap bm, MqttAppConnector appConnector) {
		mBitmap = bm;
		mAppConnector = appConnector;
	}

	@Override
	public void run() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		mBitmap.compress(CompressFormat.JPEG, 10, out);
		byte[] byteArray = out.toByteArray();
		// Bitmap decoded = BitmapFactory.decodeStream(new
		// ByteArrayInputStream(out.toByteArray()));

		String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

		if (!AppModule.hasImage(encoded)) {
			Log.v("Angel", encoded);
			mAppConnector.publishMessage("IMG|" + encoded);
		}
	}
}
