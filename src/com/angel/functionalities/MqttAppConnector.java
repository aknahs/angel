package com.angel.functionalities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public class MqttAppConnector {

	private static volatile Integer mNextMessageID = 0; // the method that

	/**
	 * Target we publish for clients to send messages to IncomingHandler. Note
	 * that calls to its binder are sequential!
	 */
	private static HandlerThread handlerThread;

	/** Messenger for communicating with the service. */
	private static Messenger mService = null;

	/** Flag indicating whether we have called bind on the service. */
	private static boolean mBound;

	/** Context of the activity from which this connector was launched */
	private static Context ctx;

	/** Name of the apk this connector is connected to */
	private static String mApk;

	public synchronized Integer getNextMessageID() {
		Integer id = mNextMessageID++;
		Log.v("MqttAppConnector", this
				+ " is generating new messageID : " + id);
		return id;
	}

	public MqttAppConnector(String apk) {
		mApk = apk;
		handlerThread = new HandlerThread("IPChandlerThread");
		handlerThread.start();
		
		Log.v("MqttAppConnector",
				"CREATED NEW INSTANCE OF MQTTAPPCONNECTOR");
	}

	public boolean isBound() {
		return mBound;
	}

	public boolean bindService(Context c) {

		if (mBound == true)
			return true;

		if (c == null) {
			Log.v("MqttAppConnector",
					"XXX Trying to bind in null context");
		}

		if (mConnection == null) {
			Log.v("MqttAppConnector",
					"XXX mconnection was null. failed to bind service");
		}

		ctx = c;

		Intent i = new Intent("com.angel.mqtt.ACTION_BIND");

		return c.getApplicationContext().bindService(i, mConnection,
				Context.BIND_AUTO_CREATE);
	}

	public void unbindService() {
		if (mBound) {
			ctx.getApplicationContext().unbindService(mConnection);
			mBound = false;
		}
	}

	/**
	 * Bind to service
	 * 
	 * @param void
	 * @return void
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {

			mService = new Messenger(service);
			
			Log.v("MqttAppConnector",
					"Binding Messenger on MqttService");
			
			Message msg = new Message();
			Bundle b = new Bundle();
			b.putString("apk", mApk);
			msg.what = MqttService.MqttMessageType.BIND.ordinal();
			msg.setData(b); // since it is IPC it cannot use the msg.obj

			try {
				Log.v("MqttAppConnector", "Sending bind message : "
						+ msg.what + " with app : " + mApk);
				mService.send(msg);
				
			} catch (RemoteException e) {
				Log.v("MqttAppConnector",
						"XXX Failed send binding message");
				e.printStackTrace();
			}

			Log.v("MqttAppConnector", "Setting bound to true");
			mBound = true;
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			Log.v("MqttAppConnector", "XXXX Service disconnected!");
			mService = null;
			mBound = false;
		}
	};

	public void publishMessage(String msg) {
		asyncSend(MqttService.MqttMessageType.PUBLISH.ordinal(), msg);
	}

	private void asyncSend(Integer what, String msg) {

		Log.v("MqttAppConnector", "AsyncSend on thread : "
				+ Thread.currentThread().getName());

		if (!mBound) {
			Log.v("MqttAppConnector",
					"XXX Attempt to asyncSend when not bound!");
			return;
		}

		Message mMsg = new Message();
		mMsg.what = what;

		Bundle b = new Bundle();
		b.putString("msg", msg);
		b.putString("apk", mApk);
		mMsg.setData(b);

		try {
			mService.send(mMsg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

}