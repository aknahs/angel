package com.angel.apps;

import java.lang.reflect.Method;

import android.app.Activity;
import android.widget.TextView;

import com.angel.functionalities.MqttConnectorEmptyActivity;

import de.robv.android.xposed.XC_MethodHook.MethodHookParam;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class StandardModule extends AppModule {

	public Method mActivityFunction = null;
	public Method mTextViewFunction = null;
	public Method mImageViewFunction = null;
	public Method mImageSpanFunction = null;
	public Method mTouchEventFunction = null;

	public StandardModule(String apkname, String mainActivity) {

		super(apkname, mainActivity);

		try {

			log("initializing functions for " + apkname);

			mActivityFunction = StandardModule.class.getDeclaredMethod(
					"standardActivityFunction", MethodHookParam.class);

			mTextViewFunction = StandardModule.class.getDeclaredMethod(
					"standardTextViewFunction", MethodHookParam.class);

			mImageViewFunction = StandardModule.class.getDeclaredMethod(
					"standardImageViewFunction", MethodHookParam.class);

			mImageSpanFunction = StandardModule.class.getDeclaredMethod(
					"standardImageSpanFunction", MethodHookParam.class);

			mTouchEventFunction = StandardModule.class.getDeclaredMethod(
					"standardTouchEventFunction", MethodHookParam.class);

			log("DONE initializing functions for " + apkname);

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handle(LoadPackageParam lpparam) {
		super.handle(lpparam);

		if (mActivityFunction != null)
			interceptActivityLaunching(lpparam, mActivityFunction);

		if (mTextViewFunction != null)
			interceptTextViews(lpparam, mTextViewFunction);

		if (mImageViewFunction != null)
			interceptImageView(lpparam, mImageViewFunction);

		if (mImageSpanFunction != null)
			interceptImageSpan(lpparam, mImageSpanFunction);

		if (mTouchEventFunction != null)
			interceptTouchEvents(lpparam, mTouchEventFunction);
	}

	public static void standardActivityFunction(MethodHookParam param) {

		String classname = (String) param.args[1];

		log("Intercepted creation of " + classname);

		if (!mAppConnector.isBound()) {
			Activity a = (Activity) param.getResult();

			param.setResult(new MqttConnectorEmptyActivity(mAppConnector, a
					.getClass()));
		}
	}

	public static void standardImageViewFunction(MethodHookParam param) {
		Object argument = param.args[0];

		processImageView(argument);
	}

	public static void standardTouchEventFunction(MethodHookParam param) {
		Object argument = param.args[0];

		processTouchEvent(param, argument);
	}

	public static void standardImageSpanFunction(MethodHookParam param) {
		Object argument = param.getResult();

		processImageSpan(argument);
	}

	public static void standardTextViewFunction(MethodHookParam param) {
		TextView etext = (TextView) param.thisObject;

		processTextView(etext);
	}
}