package com.angel.apps;

import android.app.Activity;

import com.angel.functionalities.MqttAppConnector;
import com.angel.functionalities.MqttConnectorEmptyActivity;
import com.angel.functionalities.NotificationActivity;

import de.robv.android.xposed.XC_MethodHook.MethodHookParam;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class SkypeModule extends StandardModule {

	public SkypeModule() {
		super("com.skype.raider", "activity");

		try {

			mActivityFunction = SkypeModule.class.getMethod(
					"specificSkypeActivityFunction", MethodHookParam.class,
					LoadPackageParam.class);

			// mTextViewFunction = SkypeModule.class.getMethod(
			// "specificSkypeTextViewFunction", MethodHookParam.class);
			//
			// mImageViewFunction = SkypeModule.class.getMethod(
			// "specificSkypeImageViewFunction", MethodHookParam.class);

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void handle(LoadPackageParam lpparam) {
		// super.handle(lpparam);

		if (mAppConnector == null)
			mAppConnector = new MqttAppConnector(mAppName);

		if (mActivityFunction != null)
			interceptActivityLaunching(lpparam, mActivityFunction, lpparam);
		else
			log("mActivityFunction was null");

		if (mTextViewFunction != null)
			interceptTextViews(lpparam, mTextViewFunction);

		if (mImageViewFunction != null)
			interceptImageView(lpparam, mImageViewFunction);

		if (mImageSpanFunction != null)
			interceptImageSpan(lpparam, mImageSpanFunction);

		if (mTouchEventFunction != null)
			interceptTouchEvents(lpparam, mTouchEventFunction);
	}

	public static void specificSkypeActivityFunction(MethodHookParam param,
			LoadPackageParam lpparam) {
		String classname = (String) param.args[1];

		Activity nextActivity = (Activity) param.getResult();

		log("SKYPE : Intercepted creation of " + classname);

		if (nextActivity.getClass().getName()
				.equals("com.skype.android.app.chat.ChatActivity")) {
			String message = "Chatting in Skype not allowed during curfew";

			nextActivity = new NotificationActivity("Skype",
					"com.skype.raider", message);
		}

		if (!mAppConnector.isBound()) {
			param.setResult(new MqttConnectorEmptyActivity(mAppConnector,
					nextActivity.getClass()));
		} else
			param.setResult(nextActivity);
		param.setResult(nextActivity);
	}

}
