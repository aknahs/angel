package com.angel.apps;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.angel.functionalities.MqttAppConnector;
import com.angel.functionalities.MqttConnectorEmptyActivity;
import com.angel.functionalities.NotificationActivity;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodHook.MethodHookParam;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class YTModule extends StandardModule {

	public YTModule() {
		super("com.google.android.youtube", "activity");

		try {

			mActivityFunction = YTModule.class.getMethod(
					"specificYTActivityFunction", MethodHookParam.class,
					LoadPackageParam.class);

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		//loadImages();
	}

	@Override
	public void handle(final LoadPackageParam lpparam) {
		// super.handle(lpparam);

		if (mAppConnector == null)
			mAppConnector = new MqttAppConnector(mAppName);

		if (mActivityFunction != null)
			interceptActivityLaunching(lpparam, mActivityFunction, lpparam);
		else
			log("mActivityFunction was null");


		//loadImages();

	}

	public static void specificYTActivityFunction(MethodHookParam param,
			LoadPackageParam lpparam) {
		String classname = (String) param.args[1];

		Activity nextActivity = (Activity) param.getResult();

		log("YT : Intercepted creation of " + classname);

		if (nextActivity
				.getClass()
				.getName()
				.equals("com.google.android.apps.youtube.app.WatchWhileActivity")) {

			String message = "Youtube application not allowed during curfew";

			nextActivity = new NotificationActivity("Youtube",
					"com.google.android.youtube", message, mBitmapInternet);

		}

		if (!mAppConnector.isBound()) {
			param.setResult(new MqttConnectorEmptyActivity(mAppConnector,
					nextActivity.getClass()));
		} else
			param.setResult(nextActivity);
		param.setResult(nextActivity);
	}
}
