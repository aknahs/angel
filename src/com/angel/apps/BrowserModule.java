package com.angel.apps;

import java.lang.reflect.Field;

import android.app.Activity;
import android.text.Editable;
import android.widget.TextView;

import com.angel.functionalities.NotificationActivity;

import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.XC_MethodHook.MethodHookParam;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class BrowserModule extends StandardModule {

	public BrowserModule() {
		super("com.android.browser", "activity");

		try {
			mTextViewFunction = BrowserModule.class.getMethod(
					"specificBrowserTextViewFunction", MethodHookParam.class,
					LoadPackageParam.class);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		// loadImages();
	}

	@Override
	public void handle(final LoadPackageParam lpparam) {

		if (mTextViewFunction != null)
			interceptTextViews(lpparam, mTextViewFunction, lpparam);
		else
			log("mTextViewFunction was null");

		// loadImages();

	}

	// com.android.browser.UrlInputView
	public static void specificBrowserTextViewFunction(MethodHookParam param,
			LoadPackageParam lpparam) {
		TextView etext = (TextView) param.thisObject;

		Class<?> urlInputView = XposedHelpers.findClass(
				"com.android.browser.UrlInputView", lpparam.classLoader);
		
		if(!(urlInputView.isAssignableFrom(etext.getClass())))
			return;

		Field mText;

		try {

			mText = TextView.class.getDeclaredField("mText");
			mText.setAccessible(true);
			CharSequence text = (CharSequence) mText.get(etext);

			if(text.toString().contains("youporn")){
				etext.setText("www.disney.com");
			}

		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
