package com.angel.apps;

import java.lang.reflect.Method;
import java.util.Date;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.angel.functionalities.MqttConnectorEmptyActivity;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodHook.MethodHookParam;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class FBModule extends AppModule {

	public Method mActivityFunction = null;
	public Method mTextViewFunction = null;
	public Method mImageViewFunction = null;

	public FBModule() {

		super("com.facebook.orca",
				"com.facebook.zero.upsell.activity.ZeroUpsellIntentInterstitialActivity");

		try {

			mActivityFunction = FBModule.class.getDeclaredMethod(
					"specificFacebookActivityFunction", MethodHookParam.class);

			mTextViewFunction = FBModule.class.getDeclaredMethod(
					"specificFacebookTextViewFunction", MethodHookParam.class);

			mImageViewFunction = FBModule.class.getDeclaredMethod(
					"specificFacebookImageViewFunction", MethodHookParam.class);

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handle(LoadPackageParam lpparam) {
		super.handle(lpparam);

		interceptActivityLaunching(lpparam, mActivityFunction);

		interceptTextViews(lpparam, mTextViewFunction);

		interceptImageView(lpparam, mImageViewFunction);

	}

	public static void specificFacebookActivityFunction(MethodHookParam param) {

		String classname = (String) param.args[1];

		log("Intercepted creation of " + classname);

		if (!mAppConnector.isBound()) {
			Activity a = (Activity) param.getResult();

			param.setResult(new MqttConnectorEmptyActivity(mAppConnector, a
					.getClass()));
		}
	}

	public static void specificFacebookImageViewFunction(MethodHookParam param) {
		Object argument = param.args[0];

		processImageView(argument);
	}

	public static void specificFacebookTextViewFunction(MethodHookParam param) {
		TextView etext = (TextView) param.thisObject;

		processTextView(etext);
	}
}