/*

	Title : a web front/backend for the Angel.io project
	Author : Yan Grunenberger <yan.grunenberger@telefonica.com>
	Date : July 2014

*/ 


// variable
var PORT = 3000;

// dependencies
// some events management for events coming from the phone
var events = require('events');
var eventEmitter = new events.EventEmitter();

var util = require('util');
var express = require('express');
var serveStatic = require('serve-static'); // static file for express
var morgan  = require('morgan');

// the web part
var app = express();
var server = app.listen(PORT);
var io = require('socket.io').listen(server);

// Oh Gosh. Express has started to modularize everything :-(
// Go read this https://github.com/senchalabs/connect#middleware

// the previous logger module
app.use(morgan())

// serve static files
app.use(serveStatic('public/', {'index': ['index.html', 'index.htm']}));
app.use("/js", express.static(__dirname + "/public/js"));
app.use("/img", express.static(__dirname + "/public/img"));
app.use("/css", express.static(__dirname + "/public/css"));
app.use("/partials", express.static(__dirname + "/public/partials"));
app.all("/*", function(req, res, next) {
        res.sendfile("index.html", { root: __dirname + "/public" });
    });

io.on('connection', function(socket){
  console.log('WEBSOCKET> a browser just connected');

  eventEmitter.on('appmsg', function(msg){
  	socket.emit("msg", { "message" : msg});
  });

});



console.log("Listening on port "+ PORT);

var mqtt = require('mqtt');

client = mqtt.createClient(1883, 'aknahs.system-ns.net');


// test loop 
client.subscribe('/users/andr_602ac1c0b4d82982/com.facebook.orca/frommobile');
client.subscribe('/users/andr_602ac1c0b4d82982/com.skype.raider/frommobile');
client.subscribe('/users/andr_602ac1c0b4d82982/com.whatsapp/frommobile');

var count = 0;

//setInterval(function(){
//	client.publish('/users/andr_602ac1c0b4d82982/com.facebook.orca/frommobile', 'Test mqtt message number ' + count++);
//},1000);

// a similar behavior to the adb one
client.on('message', function (topic, message) {
  console.log(util.inspect(topic));
  eventEmitter.emit('appmsg', {'topic': topic, 'message': message});
});

// Eventually closing the client
//client.end();


/*
// cleaner solution here, one day. Using the full adbkit.

var Promise = require('bluebird');
var adb = require('adbkit');
// the android part
var client = adb.createClient();

client.listDevices(function(err, devices){

	if (err) throw err;

	console.log("I have detected  " + devices.length + " devices, trying to open a channel on it...");
	for (i=0;i<devices.length;i++)
	{
		var device = devices[i];

		
	}

});*/

// quick and dirty solution : we recover the adb output, and filter by tag

//var logcat = require('adbkit-logcat');
//var spawn = require('child_process').spawn;

// Retrieve a binary log stream
//var proc = spawn('adb', ['logcat', '-B']);

// Connect logcat to the stream
//reader = logcat.readStream(proc.stdout);
//reader.on('entry', function(entry) {


	/* // example of message
{ date: Wed Jan 12 2000 06:18:56 GMT+0100 (CET),
  pid: 1819,
  tid: 1819,
  priority: 6,
  tag: 'DataRouter',
  message: 'DSR is ON. Don\'t send DTR ON.\n' }
*/
	
//	if (entry.tag == "Angel")
//  		eventEmitter.emit('adbmsg',entry.message);



//});

// Make sure we don't leave anything hanging
process.on('exit', function() {
  proc.kill();
});
