var socket = io.connect();

socket.on("msg", function(data){
	console.log(data);

	// get the message and put it on screen
	var topic = data.message.topic;
	var text = data.message.message;

	if (typeof String.prototype.startsWith != 'function') {
  		String.prototype.startsWith = function (str){
    		return this.slice(0, str.length) == str;
  		};
	}

	var strippedmessage = text.substr(4, text.length);

	if(text.startsWith("TXT")){
		$('#message').prepend($('<li class="' + topic + '">').text(strippedmessage));
	} else{
		var imgstr = '<img src="data:image/jpeg;base64,' + strippedmessage + '"/>';
		$('#message').prepend($('<li class="' + topic + '">' + imgstr));
	}

});
