var angelApp = angular.module('angelApp', [
    'ngRoute',
    'btford.socket-io',
    'angelControllers'
]);

angelApp.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
    $routeProvider.
      when('/monitor', {
        templateUrl: 'partials/monitor.html',
        controller: 'MonitorCtrl'
      }).
      /* when('/history', {
        templateUrl: 'partials/history.html',
        controller: 'HistoryCtrl'
      }). */
      when('/settings', {
        templateUrl: 'partials/settings.html',
        controller: 'SettingsCtrl'
      }).
      otherwise({
        redirectTo: '/monitor'
      });
    $locationProvider.html5Mode(true)
  }]);

angelApp.factory('socket', function(socketFactory) {
    var socket = socketFactory();
    socket.forward("msg");
    return socket;
});
