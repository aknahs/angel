var angelControllers = angular.module('angelControllers', []);

angelControllers.controller('MonitorCtrl', ['$scope', 'socket',
  function ($scope, socket) {
    $scope.orderProp = 'age';
    $scope.messages = [];

    $scope.addMessage = function(topic, type, content) {
        $scope.messages.unshift({'topic': topic, 'type': type, 'content': content});
    }

    $scope.$on("socket:msg", function(ev, data){
            // get the message and put it on screen
            var topic = data.message.topic;
            var text = data.message.message;

            if (typeof String.prototype.startsWith != 'function') {
                    String.prototype.startsWith = function (str){
                    return this.slice(0, str.length) == str;
                    };
            }

            var strippedMessage = text.substr(4, text.length);

            if (text.startsWith("TXT")){
                $scope.addMessage(topic, "txt", strippedMessage);
            } else{
                $scope.addMessage(topic, "img", strippedMessage);
            }

    });
  }]);

angelControllers.controller('SettingsCtrl', ['$scope',
  function ($scope) {
    $scope.blockedApps = {'Facebook': true, 'WhatsApp': true, 'Skype': true};

    $scope.addBlockedApp = function(name) {
        $scope.blockedApps[name] = true;
    }

    $scope.removeBlockedApp = function(name) {
        delete $scope.blockedApps[name];
    }

    $scope.blockedPersons = {'Yan Grunenberger': true, 'Pablo Serrano': true};

    $scope.addBlockedPerson = function(name) {
        $scope.blockedPersons[name] = true;
    }

    $scope.removeBlockedPerson = function(name) {
        delete $scope.blockedPersons[name];
    }

    $scope.timeBlocks = [];

    var startHour = 6;
    for (var i = 0; i < 24; i++) {
        $scope.timeBlocks.push({'label': (i % 24) + ':00', 'blocked': false});
    }

    $scope.toggleTimeBlock = function(index) {
        $scope.timeBlocks[index]['blocked'] = !$scope.timeBlocks[index]['blocked'];
    }
  }]);
